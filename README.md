1. install Node > 9.12
2. git clone `https://mandarinka69@bitbucket.org/mandarinka69/template-vue-graphql.git`
3. Open template-vue-graphql
4. Open terminal and do `npm i`
5. Start apollo-server `vue-cli-service apollo:start`
6. Open GraphQL Server on `http://localhost:4000/graphql`
7. Start vue `vue-cli-service serve`
8. The application will be running on `localhost:8080`.
